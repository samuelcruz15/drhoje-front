
import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import { HomeEscola } from './pages/escola/Home'
import { CadastrarEscola } from './pages/escola/Cadastrar'
import { EditarEscola } from './pages/escola/Editar';

import { HomeTurma } from './pages/turma/Home'
import { CadastrarTurma } from './pages/turma/Cadastrar'
import { EditarTurma } from './pages/turma/Editar'

import { HomeAlunos } from './pages/aluno/Home'
import { EditarAlunos } from './pages/aluno/Editar'
import { CadastrarAlunos } from './pages/aluno/Cadastrar'
import { VincularAlunos } from './pages/aluno/Vincular'




function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/" component={HomeEscola} />
          <Route exact path="/escola/cadastrar" component={CadastrarEscola} />
          <Route exact path="/escola/editar/:id" component={EditarEscola} />

          <Route exact path="/turma/home/:id/:nome" component={HomeTurma} />
          <Route exact path="/turma/cadastrar/:id/:nome" component={CadastrarTurma} />
          <Route exact path="/turma/editar/:id/:idTurma/:nome" component={EditarTurma} />

          <Route exact path="/alunos/home/:id/:nome/:idColegio/:nomeColegio" component={HomeAlunos} />
          <Route exact path="/alunos/cadastrar/:idTurma/:nomeTurma/:idColegio/:nomeColegio" component={CadastrarAlunos} />
          <Route exact path="/alunos/editar/:id/:idTurma/:nomeTurma/:idColegio/:nomeColegio" component={EditarAlunos} />
          <Route exact path="/alunos/vincular/:idTurma/:nomeTurma/:idColegio/:nomeColegio" component={VincularAlunos} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
