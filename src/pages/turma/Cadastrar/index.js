
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

import {
    Container, AlertSucess, AlertError, ConteudoForm, Form, Titulo, Label,
    Input, ButtonCadastrar, ConteudoTitulo, BotaoAcao, ButtonHome
} from './styles';

export const CadastrarTurma = (props) => {
    const [id] = useState(props.match.params.id);
    const [nome] = useState(props.match.params.nome);
    const [turma, setTurma] = useState({
        nome: '',
    });

    const valorInput = e => setTurma({ ...turma, [e.target.name]: e.target.value });

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    })


    const cadTurma = async e => {
        // setTurma({ ...turma, "escola_id": id });

        e.preventDefault();


        await fetch("http://localhost:8000/api/cadastrarTurma", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ turma })
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.messagem
                    });
                } else {
                    setStatus({
                        type: 'sucess',
                        mensagem: responseJson.messagem
                    });
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Falha na conexao com o banco'
                });
            });
    }

    useEffect(() => {
        setTurma({ ...turma, "escola_id": id });
    }, [])


    return (
        <Container>
            < ConteudoForm>
                <ConteudoTitulo>
                    <Titulo>Cadastrar Turma</Titulo>
                    <BotaoAcao>
                        <Link to={"/turma/home/" + id + "/" + nome}>
                            <ButtonHome>Voltar</ButtonHome>
                        </Link>
                    </BotaoAcao>
                </ConteudoTitulo>
                {status.type === 'sucess' ? <AlertSucess>{status.mensagem} </AlertSucess> : ""}
                {status.type === 'erro' ? <AlertError>{status.mensagem} </AlertError> : ""}
                <Form onSubmit={cadTurma}>
                    <Label>Nome :</Label>
                    <Input type='text' name="nome" placeholder="Nome da Turma" onChange={valorInput} required /><br /><br />
                    <Label>Complemento :</Label>
                    <Input type='text' name="complemento" placeholder="Complemento da turma (Turma A,B,C)" onChange={valorInput} required /><br /><br />
                    <Label>Ano :</Label>
                    <Input type='number' min="0" name="ano" placeholder="Ano letivo" onChange={valorInput} required /><br /><br />


                    <ButtonCadastrar type='submit'>Cadastrar</ButtonCadastrar>
                </Form>
            </ConteudoForm>
        </Container >
    );
}

