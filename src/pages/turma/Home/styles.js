import styled from 'styled-components';

export const Titulo = styled.h1`
    Color: #3e3e3e;
    font-size: 25px;
`;
export const Table = styled.table`
width: 100%;
th{
    background-color: #87CEEB;
    color: #3e3e3e;
    padding: 10px;
}
td{
    color: #3e3e3e;
    padding: 5px;
}
`;

export const Container = styled.section`
    max-width: 960px;
    margin 20px auto;
    box-shadow: 0 0 1em #6c757d;
    padding: 0px 20px 20px;
`;

export const ConteudoTitulo = styled.section`
    display: flex;
    justify-content: space-between;
`;

export const BotaoAcao = styled.section`
    margin: 30px 0px;
`;

export const Button = styled.button`
   background-color: #fff;
   padding: 8px 12px;
   border: 1px solid;
   border-radius: 4px;
   cursor: pointer;
   font-size:18px;
   :hover{
       background-color:#1E90FF;
       color:#fff;
   }
`;

export const ButtonCadastrar = styled.button`
   background-color: #fff;
   color :#008000;
   padding: 8px 12px;
   border: 1px solid #008000;
   border-radius: 4px;
   cursor: pointer;
   font-size:18px;
   :hover{
       background-color:#008000;
       color:#fff;
   }
`;


export const ButtonPrimary = styled.button`
    background-color: #fff;
    color: #0d6efd;
    padding: 5px 8px;
    border: 1px solid #0d6efd;
    border-radius: 4px;
    cursor: pointer;
    font-size: 16px;
    :hover{
        background-color: #0d6efd;
        color: #fff;
    }
`;
export const ButtonDanger = styled.button`
    background-color: #fff;
    color: #dc3545;
    padding: 5px 8px;
    border: 1px solid #dc3545;
    border-radius: 4px;
    cursor: pointer;
    font-size: 16px;
    :hover{
        background-color: #dc3545;
        color: #fff;
    }
`;

export const AlertSuccess = styled.p`
    background-color: #d1e7dd;
    color: #0f5132;
    margin: 20px 0;
    border: 1px solid #badbcc;
    border-radius: 4px;
    padding: 7px;
`;

export const AlertDanger = styled.p`
    background-color: #f8d7da;
    color: #842029;
    margin: 20px 0;
    border: 1px solid #f5c2c7;
    border-radius: 4px;
    padding: 7px;
`;

