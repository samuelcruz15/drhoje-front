
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';

import {
    Container, ConteudoTitulo, BotaoAcao, Button, ButtonCadastrar, Titulo, Table,
    ButtonPrimary, ButtonDanger, AlertSuccess, AlertDanger
} from './styles';

export const HomeTurma = (props) => {

    const [nome] = useState(props.match.params.nome);
    const [id] = useState(props.match.params.id);
    const [data, setData] = useState([]);

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });


    const getTurma = async () => {

        fetch("http://localhost:8000/api/visualizarTurmas/" + id)
            .then((response) => response.json())
            .then((responseJson) => (
                setData(responseJson)
            ));
    }
    const apagarAluno = async (turmaId) => {
        await fetch("http://localhost:8000/api/deleteTurma/" + turmaId, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(turmaId)
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: responseJson.mensagem
                    });
                    getTurma();
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: "Erro: Não foi possível estabelecer conexão"
                });
            });
    };

    useEffect(() => {
        getTurma();
    }, [])


    return (
        <Container>
            <ConteudoTitulo>
                <Titulo>Listagem das Turmas - Colégio : {nome} </Titulo>
                <BotaoAcao>
                    <Link to={"/turma/cadastrar/" + id + "/" + nome}>
                        <ButtonCadastrar>Cadastrar Turma</ButtonCadastrar>
                    </Link> {" "}
                    <Link to="/">
                        <Button>Voltar Escola</Button>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>
            { status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
            { status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Complemento</th>
                        <th>Ano</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {Object.values(data).map(turmas => (
                        <tr key={turmas.id}>
                            <td>{turmas.id}</td>
                            <td>{turmas.nome}</td>
                            <td>{turmas.complemento}</td>
                            <td>{turmas.ano}</td>
                            <td> <Link to={"/turma/editar/" + turmas.id + "/" + id + "/" + nome}>
                                <ButtonPrimary>Editar</ButtonPrimary>
                            </Link>
                                {" "}
                                <ButtonDanger onClick={() => { if (window.confirm('Deseja realmente apagar essa turma?')) apagarAluno(turmas.id) }}>Apagar</ButtonDanger>
                                {" "} <Link to={"/alunos/home/" + turmas.id + "/" + turmas.nome + "/" + id + "/" + nome}>
                                    <ButtonPrimary>Mostrar Alunos</ButtonPrimary>
                                </Link>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container >
    );
}

