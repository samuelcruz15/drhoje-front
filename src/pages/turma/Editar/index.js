import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { Container, ConteudoForm, ConteudoTitulo, Titulo, BotaoAcao, ButtonInfo, AlertSuccess, AlertDanger, Form, Label, Input, ButtonWarning } from './styles';

export const EditarTurma = (props) => {

    const [id] = useState(props.match.params.id);
    const [idTurma] = useState(props.match.params.idTurma);
    const [nomeEscola] = useState(props.match.params.nome);

    const [nome, setNome] = useState('');
    const [complemento, setComplemento] = useState('');
    const [ano, setAno] = useState('');


    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    })

    const editTurma = async e => {
        e.preventDefault();

        await fetch("http://localhost:8000/api/editarTurma/" + id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id, nome, complemento, ano })
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: responseJson.mensagem
                    });
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: "Erro na conexão, tente mais tarde!"
                });
            });
    }

    useEffect(() => {
        const getTurma = async () => {
            await fetch("http://localhost:8000/api/visualizarTurma/" + id)
                .then((response) => response.json())
                .then((responseJson) => {
                    setNome(responseJson.nome);
                    setComplemento(responseJson.complemento);
                    setAno(responseJson.ano);
                });
        }
        getTurma();
    }, [id]);

    return (
        <Container>
            <ConteudoForm>
                <ConteudoTitulo>
                    <Titulo>Edição Turma</Titulo>
                    <BotaoAcao>
                        <Link to={"/turma/home/" + idTurma + "/" + nomeEscola}>
                            <ButtonInfo>Home</ButtonInfo>
                        </Link>
                    </BotaoAcao>
                </ConteudoTitulo>

                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}


                <Form onSubmit={editTurma}>
                    <Label>Nome </Label>
                    <Input type="text" name="nome" placeholder="Nome do Aluno" value={nome} onChange={e => setNome(e.target.value)} required />
                    <Label>Complemento :</Label>
                    <Input type='text' name="complemento" placeholder="Complemento da turma (Turma A,B,C)" value={complemento} onChange={e => setComplemento(e.target.value)} required /><br /><br />
                    <Label>Ano :</Label>
                    <Input type='number' min="0" name="ano" placeholder="Ano letivo" value={ano} onChange={e => setAno(e.target.value)} required /><br /><br />

                    <ButtonWarning type="submit">Editar</ButtonWarning>
                </Form>

            </ConteudoForm>
        </Container>
    );
}