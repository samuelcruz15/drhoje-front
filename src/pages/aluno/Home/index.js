
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';

import {
    Container, ConteudoTitulo, BotaoAcao, Button, ButtonCadastrar, Titulo, Table,
    ButtonPrimary, ButtonDanger, ButtonDesvin, ButtonVincular, AlertSuccess, AlertDanger
} from './styles';

export const HomeAlunos = (props) => {

    const [nome] = useState(props.match.params.nome);
    const [id] = useState(props.match.params.id);
    const [nomeColegio] = useState(props.match.params.nomeColegio);
    const [idColegio] = useState(props.match.params.idColegio);
    const [data, setData] = useState([]);

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });


    const getAluno = async () => {

        fetch("http://localhost:8000/api/visualizarAlunos/" + id)
            .then((response) => response.json())
            .then((responseJson) => (
                setData(responseJson)
            ));
    }
    const apagarAluno = async (alunoId) => {
        await fetch("http://localhost:8000/api/deleteAluno/" + alunoId, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(alunoId)
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: responseJson.mensagem
                    });
                    getAluno();
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: "Erro: Não foi possível estabelecer conexão"
                });
            });
    };

    const desvincularAluno = async (alunoId) => {
        await fetch("http://localhost:8000/api/transferirAluno/" + alunoId, {
            method: 'put',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(alunoId)
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: responseJson.mensagem
                    });
                    getAluno();
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: "Erro: Não foi possível estabelecer conexão"
                });
            });
    };

    useEffect(() => {
        getAluno();
    }, [])


    return (
        <Container>
            <ConteudoTitulo>
                <Titulo>Listagem dos Alunos da Turma : {nome} </Titulo>
                <BotaoAcao>
                    <Link to={"/alunos/vincular/" + id + "/" + nome + "/" + idColegio + "/" + nomeColegio}>
                        <ButtonVincular>Vincular Aluno</ButtonVincular>
                    </Link> {" "}
                    <Link to={"/alunos/cadastrar/" + id + "/" + nome + "/" + idColegio + "/" + nomeColegio}>
                        <ButtonCadastrar>Cadastrar Aluno</ButtonCadastrar>
                    </Link> {" "}
                    <Link to={"/turma/home/" + idColegio + "/" + nomeColegio}>
                        <Button>Voltar Turma</Button>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>
            { status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
            { status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Nascimento</th>
                        <th>Sexo</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {Object.values(data).map(alunos => (
                        <tr key={alunos.id}>
                            <td>{alunos.id}</td>
                            <td>{alunos.nome}</td>
                            <td>{alunos.data_nascimento}</td>
                            <td>{alunos.sexo}</td>
                            <td> <Link to={"/alunos/editar/" + alunos.id + "/" + id + "/" + nome + "/" + idColegio + "/" + nomeColegio}>
                                <ButtonPrimary>Editar</ButtonPrimary>
                            </Link>
                                {" "}
                                <ButtonDanger onClick={() => { if (window.confirm('Deseja realmente apagar este Aluno?')) apagarAluno(alunos.id) }}>Apagar</ButtonDanger>
                                {" "}
                                <ButtonDesvin onClick={() => { if (window.confirm('Deseja realmente Transferir este Aluno da turma?')) desvincularAluno(alunos.id) }}>Desvincular</ButtonDesvin>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container >
    );
}

