import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import {
    Container, ConteudoForm, ConteudoTitulo, Titulo, BotaoAcao,
    ButtonInfo, AlertSuccess, AlertDanger, Form, Label, ButtonVincular, Select
} from './styles';

export const VincularAlunos = (props) => {

    const [nomeTurma] = useState(props.match.params.nomeTurma);
    const [turma_id] = useState(props.match.params.idTurma);
    const [nomeColegio] = useState(props.match.params.nomeColegio);
    const [idColegio] = useState(props.match.params.idColegio);

    const [idAluno, setId] = useState('');
    const [data, setData] = useState([]);


    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    })

    const vincularAluno = async e => {
        e.preventDefault();

        await fetch("http://localhost:8000/api/vincularAluno/" + idAluno, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ turma_id })
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: responseJson.mensagem
                    });
                    getTurma();
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: "Erro na conexão, tente mais tarde!"
                });
            });
    }

    const getTurma = async () => {
        console.log('s');
        await fetch("http://localhost:8000/api/visualizarAlunosTransferidos/")
            .then((response) => response.json())
            .then((responseJson) => {
                setData(responseJson)
            });
    }

    useEffect(() => {
        getTurma();
    }, []);



    return (
        <Container>
            <ConteudoForm>
                <ConteudoTitulo>
                    <Titulo>Vincular Aluno - Turma {nomeTurma}</Titulo>
                    <BotaoAcao>
                        <Link to={"/alunos/home/" + turma_id + "/" + nomeTurma + "/" + idColegio + "/" + nomeColegio}>
                            <ButtonInfo>Voltar Turma</ButtonInfo>
                        </Link>
                    </BotaoAcao>
                </ConteudoTitulo>

                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}


                <Form onSubmit={vincularAluno}>
                    <Label>Aluno </Label>
                    <Select name="nome" onChange={e => setId(e.target.value)} required >
                        <option value=""></option>
                        {Object.values(data).map(alunos => (
                            <option value={alunos.id}>{alunos.nome}</option>
                        ))}
                    </Select> {""}
                    <ButtonVincular type="submit">Vincular</ButtonVincular>
                </Form>

            </ConteudoForm>
        </Container>
    );
}