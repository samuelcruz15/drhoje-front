
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

import {
    Container, AlertSucess, AlertError, ConteudoForm, Form, Titulo, Label,
    Input, ButtonCadastrar, ConteudoTitulo, BotaoAcao, ButtonHome
} from './styles';

export const CadastrarAlunos = (props) => {
    const [nomeTurma] = useState(props.match.params.nomeTurma);
    const [idTurma] = useState(props.match.params.idTurma);
    const [nomeColegio] = useState(props.match.params.nomeColegio);
    const [idColegio] = useState(props.match.params.idColegio);

    const [aluno, setAluno] = useState({
        nome: '',
    });

    const valorInput = e => setAluno({ ...aluno, [e.target.name]: e.target.value });

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    })


    const cadTurma = async e => {

        e.preventDefault();


        await fetch("http://localhost:8000/api/cadastrarAluno", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ aluno })
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.messagem
                    });
                } else {
                    setStatus({
                        type: 'sucess',
                        mensagem: responseJson.messagem
                    });
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Falha na conexao com o banco'
                });
            });
    }

    useEffect(() => {
        setAluno({ ...aluno, "turma_id": idTurma });
    }, [])


    return (
        <Container>
            < ConteudoForm>
                <ConteudoTitulo>
                    <Titulo>Cadastrar Aluno</Titulo>
                    <BotaoAcao>
                        <Link to={"/alunos/home/" + idTurma + "/" + nomeTurma + "/" + idColegio + "/" + nomeColegio}>
                            <ButtonHome>Voltar Turma</ButtonHome>
                        </Link>
                    </BotaoAcao>
                </ConteudoTitulo>
                {status.type === 'sucess' ? <AlertSucess>{status.mensagem} </AlertSucess> : ""}
                {status.type === 'erro' ? <AlertError>{status.mensagem} </AlertError> : ""}
                <Form onSubmit={cadTurma}>
                    <Label>Nome :</Label>
                    <Input type='text' name="nome" placeholder="Nome do Aluno" onChange={valorInput} required /><br /><br />
                    <Label>Nascimento :</Label>
                    <Input type='date' name="data_nascimento" placeholder="Data de Nascimento" onChange={valorInput} required /><br /><br />
                    <Label>Sexo :</Label>
                    <Input type='string' name="sexo" placeholder="Sexo F ou M" onChange={valorInput} required /><br /><br />


                    <ButtonCadastrar type='submit'>Cadastrar</ButtonCadastrar>
                </Form>
            </ConteudoForm>
        </Container >
    );
}

