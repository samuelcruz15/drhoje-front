import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { Container, ConteudoForm, ConteudoTitulo, Titulo, BotaoAcao, ButtonInfo, AlertSuccess, AlertDanger, Form, Label, Input, ButtonWarning } from './styles';

export const EditarAlunos = (props) => {

    const [id] = useState(props.match.params.id);
    const [nomeTurma] = useState(props.match.params.nomeTurma);
    const [idTurma] = useState(props.match.params.idTurma);
    const [nomeColegio] = useState(props.match.params.nomeColegio);
    const [idColegio] = useState(props.match.params.idColegio);

    const [nome, setNome] = useState('');
    const [data_nascimento, setData_nascimento] = useState('');
    const [sexo, setSexo] = useState('');
    const [statusAluno, setStatusAluno] = useState('');


    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    })

    const editAluno = async e => {
        e.preventDefault();

        await fetch("http://localhost:8000/api/editarAluno/" + id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id, nome, data_nascimento, sexo, statusAluno })
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: responseJson.mensagem
                    });
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: "Erro na conexão, tente mais tarde!"
                });
            });
    }

    useEffect(() => {
        const getTurma = async () => {
            await fetch("http://localhost:8000/api/visualizarAluno/" + id)
                .then((response) => response.json())
                .then((responseJson) => {
                    setNome(responseJson.nome);
                    setData_nascimento(responseJson.data_nascimento);
                    setSexo(responseJson.sexo);
                    setStatusAluno(responseJson.status);
                });
        }
        getTurma();
    }, [id]);

    return (
        <Container>
            <ConteudoForm>
                <ConteudoTitulo>
                    <Titulo>Edição Aluno</Titulo>
                    <BotaoAcao>
                        <Link to={"/alunos/home/" + idTurma + "/" + nomeTurma + "/" + idColegio + "/" + nomeColegio}>
                            <ButtonInfo>Voltar Turma</ButtonInfo>
                        </Link>
                    </BotaoAcao>
                </ConteudoTitulo>

                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}


                <Form onSubmit={editAluno}>
                    <Label>Nome </Label>
                    <Input type="text" name="nome" placeholder="Nome do Aluno" value={nome} onChange={e => setNome(e.target.value)} required />
                    <Label>Nascimento :</Label>
                    <Input type='date' name="data_nascimento" placeholder="Data de nascimento" value={data_nascimento} onChange={e => setData_nascimento(e.target.value)} required /><br /><br />
                    <Label>Sexo :</Label>
                    <Input type='text' name="sexo" placeholder="Sexo do Aluno" value={sexo} onChange={e => setSexo(e.target.value)} required /><br /><br />
                    <Label>Status :</Label>
                    <Input type='text' name="status" value={statusAluno} readOnly /><br /><br />

                    <ButtonWarning type="submit">Editar</ButtonWarning>
                </Form>

            </ConteudoForm>
        </Container>
    );
}