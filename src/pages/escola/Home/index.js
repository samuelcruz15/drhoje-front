
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';

import {
    Container, ConteudoTitulo, BotaoAcao, Button, Titulo, Table,
    ButtonPrimary, ButtonDanger, ButtonTurmas, AlertSuccess, AlertDanger
} from './styles';

export const HomeEscola = () => {

    const [data, setData] = useState([]);

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    });


    const getEscola = async () => {
        fetch("http://localhost:8000/api/home")
            .then((response) => response.json())
            .then((responseJson) => (
                setData(responseJson)
            ));
    }
    const apagarEscola = async (escolaId) => {
        await fetch("http://localhost:8000/api/deleteEscola/" + escolaId, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(escolaId)
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: responseJson.mensagem
                    });
                    getEscola();
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: "Erro: Não foi possível estabelecer conexão"
                });
            });
    };

    useEffect(() => {
        getEscola();
    }, [])


    return (
        <Container>
            <ConteudoTitulo>
                <Titulo>Listagem das Escolas</Titulo>
                <BotaoAcao>
                    <Link to="/escola/cadastrar">
                        <Button>Cadastrar Escola</Button>
                    </Link>
                </BotaoAcao>
            </ConteudoTitulo>
            {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
            {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}
            <Table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {Object.values(data).map(escolas => (
                        <tr key={escolas.id}>
                            <td>{escolas.id}</td>
                            <td>{escolas.nome}</td>
                            <td> <Link to={"/escola/editar/" + escolas.id}>
                                <ButtonPrimary>Editar</ButtonPrimary>
                            </Link>
                                {" "}
                                <ButtonDanger onClick={() => { if (window.confirm('Deseja realmente apagar a escola?')) apagarEscola(escolas.id) }}>Apagar</ButtonDanger>
                                {" "}<Link to={"/turma/home/" + escolas.id + "/" + escolas.nome}>
                                    <ButtonTurmas>Mostrar Turmas</ButtonTurmas>
                                </Link>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </Container>
    );
}

