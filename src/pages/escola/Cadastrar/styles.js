import styled from 'styled-components';

export const Container = styled.section`
    max-width: 960px;
    margin 20px auto;
    box-shadow: 0 0 1em #6c757d;
`;
export const Titulo = styled.h1`
    Color: #3e3e3e;
    font-size: 25px;
`;

export const AlertSucess = styled.p`
    background-color: #d1e7dd;
    Color: #0f5132;
    margin: 20px 0;
    border: 1px solid #badbcc;
    border-radius: 4px;
    padding: 7px;
`;

export const AlertError = styled.p`
    background-color: #f8d7da;
    Color: #842029;
    margin: 20px 0;
    border: 1px solid #badbcc;
    border-radius: 4px;
    padding: 7px;
`;

export const ConteudoForm = styled.section`
    max-width: 960px;
    padding: 10px 30px 30px;
`;

export const Form = styled.form`
    margin: 0px auto;
`;

export const Label = styled.label`
    width: 100%;
    padding: 12px;
    margin-top: 6px;
    margin-bottom: 16px;
`;

export const Input = styled.input`
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    margin-top: 6px;
    margin-bottom: 16px;
    resize:vertical;
`;

export const ButtonCadastrar = styled.button`
   background-color: #fff;
   padding: 8px 12px;
   border: 1px solid;
   border-radius: 4px;
   cursor: pointer;
   font-size:18px;
   :hover{
       background-color:#1E90FF;
       color:#fff;
   }
`;

export const ConteudoTitulo = styled.section`
    display: flex;
    justify-content: space-between;
`;

export const BotaoAcao = styled.section`
    margin: 30px 0px;
`;

export const ButtonHome = styled.button`
   background-color: #fff;
   collor: #0dcaf0;
   padding: 6px 10px;
   border: 1px solid #0dcaf0;
   border-radius: 4px;
   cursor: pointer;
   font-size:15px;
   :hover{
       background-color:#31d2f2;
       color:#fff;
   }
`;

