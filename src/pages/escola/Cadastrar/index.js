
import React, { useState } from 'react'
import { Link } from 'react-router-dom'

import {
    Container, AlertSucess, AlertError, ConteudoForm, Form, Titulo, Label,
    Input, ButtonCadastrar, ConteudoTitulo, BotaoAcao, ButtonHome
} from './styles';

export const CadastrarEscola = () => {

    const [escola, setEscola] = useState({
        nome: '',
    });

    const valorInput = e => setEscola({ ...escola, [e.target.name]: e.target.value });

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    })


    const cadProduto = async e => {
        e.preventDefault();

        await fetch("http://localhost:8000/api/cadastrarEscola", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ escola })
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.messagem
                    });
                } else {
                    setStatus({
                        type: 'sucess',
                        mensagem: responseJson.messagem
                    });
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: 'Falha na conexao com o banco'
                });
            });
    }


    return (
        <Container>
            < ConteudoForm>
                <ConteudoTitulo>
                    <Titulo>Cadastrar Escola</Titulo>
                    <BotaoAcao>
                        <Link to="/">
                            <ButtonHome>Home </ButtonHome>
                        </Link>
                    </BotaoAcao>
                </ConteudoTitulo>
                {status.type === 'sucess' ? <AlertSucess>{status.mensagem} </AlertSucess> : ""}
                {status.type === 'erro' ? <AlertError>{status.mensagem} </AlertError> : ""}
                <Form onSubmit={cadProduto}>
                    <Label>Nome da Escola:</Label>
                    <Input type='text' name="nome" placeholder="Nome da Instituição" onChange={valorInput} required /><br /><br />

                    <ButtonCadastrar type='submit'>Cadastrar</ButtonCadastrar>
                </Form>
            </ConteudoForm>
        </Container >
    );
}

