import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import {
    Container, ConteudoForm, ConteudoTitulo, Titulo, BotaoAcao
    , ButtonInfo, AlertSuccess, AlertDanger, Form, Label, Input, ButtonWarning
} from './styles';

export const EditarEscola = (props) => {

    const [id] = useState(props.match.params.id);

    const [nome, setNome] = useState('');

    const [status, setStatus] = useState({
        type: '',
        mensagem: ''
    })

    const editEscola = async e => {
        e.preventDefault();

        await fetch("http://localhost:8000/api/editarEscola/" + id, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id, nome })
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.erro) {
                    setStatus({
                        type: 'erro',
                        mensagem: responseJson.mensagem
                    });
                } else {
                    setStatus({
                        type: 'success',
                        mensagem: responseJson.mensagem
                    });
                }
            }).catch(() => {
                setStatus({
                    type: 'erro',
                    mensagem: "Erro na conexão, tente mais tarde!"
                });
            });
    }

    useEffect(() => {
        const getEscola = async () => {
            await fetch("http://localhost:8000/api/visualizarEscola/" + id)

                .then((response) => response.json())
                .then((responseJson) => {
                    setNome(responseJson.nome);
                });
        }
        getEscola();
    }, [id]);

    return (
        <Container>
            <ConteudoForm>
                <ConteudoTitulo>
                    <Titulo>Edição Escola</Titulo>
                    <BotaoAcao>
                        <Link to="/">
                            <ButtonInfo>Home</ButtonInfo>
                        </Link>
                    </BotaoAcao>
                </ConteudoTitulo>

                {status.type === 'erro' ? <AlertDanger>{status.mensagem}</AlertDanger> : ""}
                {status.type === 'success' ? <AlertSuccess>{status.mensagem}</AlertSuccess> : ""}


                <Form onSubmit={editEscola}>
                    <Label>Nome da Escola: </Label>
                    <Input type="text" name="nome" placeholder="Nome da Instituição" value={nome} onChange={e => setNome(e.target.value)} required />

                    <ButtonWarning type="submit">Editar</ButtonWarning>
                </Form>

            </ConteudoForm>
        </Container>
    );
}